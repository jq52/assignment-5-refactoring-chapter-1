public class PerformanceCalculator {
    public Performance performance;
    public Play play;

    public PerformanceCalculator(Performance aPerformance, Play aplay) {
        this.performance = aPerformance;
        this.play = aplay;
    }
    public int get_amount(){
        throw new IllegalArgumentException("unknown type: " +  this.play.getType());
    }
    public int get_volumeCredits(){
        return Math.max(this.performance.getAudience() - 30, 0);
    }

}

class TragedyCalculator extends PerformanceCalculator {
    public TragedyCalculator(Performance aPerformance, Play aplay) {
        super(aPerformance, aplay);
    }
    public int get_amount(){
        int thisAmount = 40000;
        if (this.performance.getAudience() > 30) {
            thisAmount += 1000 * (this.performance.getAudience() - 30);
        }
        return thisAmount;
    }

}

class ComedyCalculator extends PerformanceCalculator {
    public ComedyCalculator(Performance aPerformance, Play aplay) {
        super(aPerformance, aplay);
    }
    public int get_amount(){
        int thisAmount = 30000;
        if (this.performance.getAudience() > 20) {
            thisAmount += 10000 + 500 * (this.performance.getAudience() - 20);
        }
        thisAmount += 300 * this.performance.getAudience();
        return thisAmount;
    }
    public int get_volumeCredits(){
        return (int)(super.get_volumeCredits() +Math.floor(this.performance.getAudience() / 5));
    }

}

