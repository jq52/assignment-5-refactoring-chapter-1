import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	public Play playFor(Performance perf) {
		return plays.get(perf.getPlayID());
	}


	public int AmountFor(Performance perf){
		/*
		int thisAmount = 0;

		switch (playFor(perf).getType()) {
			case "tragedy": thisAmount = 40000;
				if (perf.getAudience() > 30) {
					thisAmount += 1000 * (perf.getAudience() - 30);
				}
				break;
			case "comedy":  thisAmount = 30000;
				if (perf.getAudience() > 20) {
					thisAmount += 10000 + 500 * (perf.getAudience() - 20);
				}
				thisAmount += 300 * perf.getAudience();
				break;
			default:        throw new IllegalArgumentException("unknown type: " +  playFor(perf).getType());
		}
		return thisAmount;
		 */
		return new PerformanceCalculator(perf, playFor(perf)).get_amount();
	}

	public int volumeCreditsFor(Performance perf) {
		int volumeCredits = 0;
		volumeCredits += Math.max(perf.getAudience() - 30, 0);
		if ( playFor(perf).getType().equals("comedy")) {
			volumeCredits += Math.floor((double) perf.getAudience() / 5.0);
		}
		return volumeCredits;
	}

	public String usd(int thisAmount) {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		return numberFormat.format((double) thisAmount / 100);
	}

	public int totalVolumeCredits(statementData data) {
		int volumeCredits = 0;
		for (Performance perf : data.performances) {
			volumeCredits += perf.volumeCredits;
		}
		return volumeCredits;
	}

	public int totalAmount(statementData data) {
		int totalAmount = 0;
		for (Performance perf: data.performances) {
			totalAmount += perf.amount;
		}
		return totalAmount;
	}

	public String statement(){
		return renderPlainText(createStatementData());
	}

	public statementData createStatementData(){
		statementData data = new statementData(customer,performances);
		for(Performance aPerformance: data.performances){
			aPerformance = enrichPerformance(aPerformance);
		}
		data.totalAmount =totalAmount(data);
		data.totalVolumeCredits = totalVolumeCredits(data);

		return data;
	}

	public String htmlStatement() {
		return renderHtml(createStatementData());
	}

	public String renderHtml (statementData data) {
		String result = "<h1>Statement for " + data.customer +"</h1>\n";
		result += "<table>\n";
		result += "<tr><th>play</th><th>seats</th><th>cost</th></tr>";
		for (Performance perf: data.performances) {
			result += "  <tr><td>"+perf.play.getName()+"</td><td>"+perf.getAudience()+"</td>`";
			result += "<td>"+usd(perf.amount)+"</td></tr>\n";
		}
		result += "</table>\n";
		result += "<p>Amount owed is <em>" +usd(data.totalAmount)+"</em></p>\n";
		result += "<p>You earned <em>"+data.totalVolumeCredits+"</em> credits</p>\n";
		return result;
	}


	public Performance enrichPerformance(Performance aPerformance) {
		//aPerformance.play = playFor(aPerformance);
		PerformanceCalculator calculator = createPerformanceCalculator(aPerformance,playFor(aPerformance));
		aPerformance.play = calculator.play;
		aPerformance.amount = calculator.get_amount();
		aPerformance.volumeCredits = calculator.get_volumeCredits();
		return aPerformance;
	}

	public String renderPlainText(statementData data) {
		//DecimalFormat numberFormat = new DecimalFormat("#.00");

		String result = "Statement for " + this.customer + "\n";
		for (Performance perf: data.performances) {
			//Play play = playFor(perf);
			if (perf.play == null) {
				throw new IllegalArgumentException("No play found");
			}
			// print line for this order
			result += "  " + perf.play.getName() + ": $" +usd(perf.amount) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}
		result += "Amount owed is $" + usd(data.totalAmount) + "\n";
		result += "You earned " + data.totalVolumeCredits + " credits" + "\n";
		return result;
	}

	public PerformanceCalculator createPerformanceCalculator(Performance aPerformance, Play aPlay){
		switch(aPlay.getType()) {
			case "tragedy": return new TragedyCalculator(aPerformance, aPlay);
			case "comedy" : return new ComedyCalculator(aPerformance, aPlay);
			default:
				throw new IllegalArgumentException("Unknown type:" + aPlay.getType());
		}
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
